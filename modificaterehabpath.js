var superagent = require('superagent');
var config = require('./config');
var j = require('jquery');

// 数据列表
var implListData = [
    {
        "id": "9",
        "phase": "1",
        "cycle": "2",
        "start": "2",
        "length": "3",
        "patient_category_id": "4",
        "task_type": "1",
        "task_name": "病情评估",
        "task_title": "72h结束时系统发起第一阶段评估（发送表一问题4-13）",
        "task_detail": "",
        "ref_id": "2"
    },
    {
        "id": "17",
        "phase": "2",
        "cycle": "2",
        "start": "5",
        "length": "26",
        "patient_category_id": "4",
        "task_type": "1",
        "task_name": "病情评估",
        "task_title": "1月结束时康复第二阶段评估",
        "task_detail": "",
        "ref_id": "3"
    },
    {
        "id": "25",
        "phase": "3",
        "cycle": "2",
        "start": "31",
        "length": "60",
        "patient_category_id": "4",
        "task_type": "1",
        "task_name": "病情评估",
        "task_title": "3月结束时系统发起随访（发送表一问题14-24）",
        "task_detail": "",
        "ref_id": "1"
    },
    {
        "id": "28",
        "phase": "3",
        "cycle": "2",
        "start": "91",
        "length": "90",
        "patient_category_id": "4",
        "task_type": "1",
        "task_name": "病情评估",
        "task_title": "6月结束时系统发起随访（发送表一问题25-35）",
        "task_detail": "",
        "ref_id": "1"
    },
    {
        "id": "31",
        "phase": "3",
        "cycle": "2",
        "start": "181",
        "length": "180",
        "patient_category_id": "4",
        "task_type": "1",
        "task_name": "病情评估",
        "task_title": "12月结束时系统发起随访（发送表一问题36-46）",
        "task_detail": "",
        "ref_id": "1"
    },
    {
        "id": "34",
        "phase": "3",
        "cycle": "2",
        "start": "361",
        "length": "180",
        "patient_category_id": "4",
        "task_type": "1",
        "task_name": "病情评估",
        "task_title": "18月结束时系统发起随访（发送表一问题47-55）",
        "task_detail": "",
        "ref_id": "1"
    },
    {
        "id": "37",
        "phase": "3",
        "cycle": "2",
        "start": "541",
        "length": "180",
        "patient_category_id": "4",
        "task_type": "1",
        "task_name": "病情评估",
        "task_title": "24月结束时系统发起康复第三阶段评估（发送表一问题56-64）",
        "task_detail": "",
        "ref_id": "1"
    },
    {
        "id": "2",
        "phase": "1",
        "cycle": "2",
        "start": "1",
        "length": "1",
        "patient_category_id": "2",
        "task_type": "2",
        "task_name": "用药管理",
        "task_title": "hahah",
        "task_detail": "直肠给药，一次1粒，一日1-2次5ci5ci5ci",
        "ref_id": "0"
    },
    {
        "id": "3",
        "phase": "1",
        "cycle": "2",
        "start": "1",
        "length": "1",
        "patient_category_id": "2",
        "task_type": "2",
        "task_name": "用药管理",
        "task_title": "金刚藤",
        "task_detail": "口服,一次4粒，一日3次",
        "ref_id": "0"
    },
    {
        "id": "4",
        "phase": "1",
        "cycle": "2",
        "start": "1",
        "length": "1",
        "patient_category_id": "2",
        "task_type": "2",
        "task_name": "用药管理",
        "task_title": "益气维血",
        "task_detail": "口服。成人一次1包，一日3次",
        "ref_id": "0"
    },
    {
        "id": "10",
        "phase": "1",
        "cycle": "2",
        "start": "2",
        "length": "3",
        "patient_category_id": "2",
        "task_type": "2",
        "task_name": "用药管理",
        "task_title": "康复消炎栓",
        "task_detail": "直肠给药，一次1粒，一日1-2次",
        "ref_id": "0"
    },
    {
        "id": "11",
        "phase": "1",
        "cycle": "2",
        "start": "2",
        "length": "3",
        "patient_category_id": "2",
        "task_type": "2",
        "task_name": "用药管理",
        "task_title": "金刚藤",
        "task_detail": "口服,一次4粒，一日3次",
        "ref_id": "0"
    },
    {
        "id": "12",
        "phase": "1",
        "cycle": "2",
        "start": "2",
        "length": "3",
        "patient_category_id": "2",
        "task_type": "2",
        "task_name": "用药管理",
        "task_title": "益气维血",
        "task_detail": "口服。成人一次1包，一日3次",
        "ref_id": "0"
    },
    {
        "id": "18",
        "phase": "2",
        "cycle": "2",
        "start": "5",
        "length": "26",
        "patient_category_id": "2",
        "task_type": "2",
        "task_name": "用药管理",
        "task_title": "康复消炎栓",
        "task_detail": "直肠给药，一次1粒，一日1-2次",
        "ref_id": "0"
    },
    {
        "id": "19",
        "phase": "2",
        "cycle": "2",
        "start": "5",
        "length": "26",
        "patient_category_id": "2",
        "task_type": "2",
        "task_name": "用药管理",
        "task_title": "金刚藤",
        "task_detail": "口服,一次4粒，一日3次",
        "ref_id": "0"
    },
    {
        "id": "20",
        "phase": "2",
        "cycle": "2",
        "start": "5",
        "length": "26",
        "patient_category_id": "2",
        "task_type": "2",
        "task_name": "用药管理",
        "task_title": "益气维血",
        "task_detail": "口服。成人一次1包，一日3次",
        "ref_id": "0"
    },
    {
        "id": "21",
        "phase": "2",
        "cycle": "2",
        "start": "5",
        "length": "26",
        "patient_category_id": "1",
        "task_type": "4",
        "task_name": "检查检验",
        "task_title": "门诊复查",
        "task_detail": "复查患者有无盆腔炎，提醒疏通手术患者，再次造影。如果术中有病理，上传病理结果：血常规、白带常规、B超",
        "ref_id": "0"
    },
    {
        "id": "5",
        "phase": "1",
        "cycle": "2",
        "start": "1",
        "length": "1",
        "patient_category_id": "3",
        "task_type": "5",
        "task_name": "日常数据",
        "task_title": "体温计自测体温",
        "task_detail": "一日5次，取最高值",
        "ref_id": "4"
    },
    {
        "id": "13",
        "phase": "1",
        "cycle": "2",
        "start": "2",
        "length": "3",
        "patient_category_id": "3",
        "task_type": "5",
        "task_name": "日常数据",
        "task_title": "体温计自测体温",
        "task_detail": "一日5次，取最高值",
        "ref_id": "4"
    },
    {
        "id": "26",
        "phase": "3",
        "cycle": "2",
        "start": "31",
        "length": "60",
        "patient_category_id": "3",
        "task_type": "5",
        "task_name": "日常数据",
        "task_title": "监测体温以判断排卵情况（患者自采最低温，临床上已接受使用电子体温计“孕律”）",
        "task_detail": "",
        "ref_id": "5"
    },
    {
        "id": "29",
        "phase": "3",
        "cycle": "2",
        "start": "91",
        "length": "90",
        "patient_category_id": "3",
        "task_type": "5",
        "task_name": "日常数据",
        "task_title": "监测体温以判断排卵情况（患者自采最低温，临床上已接受使用电子体温计“孕律”）",
        "task_detail": "",
        "ref_id": "5"
    },
    {
        "id": "32",
        "phase": "3",
        "cycle": "2",
        "start": "181",
        "length": "180",
        "patient_category_id": "3",
        "task_type": "5",
        "task_name": "日常数据",
        "task_title": "监测体温以判断排卵情况（患者自采最低温，临床上已接受使用电子体温计“孕律”）",
        "task_detail": "",
        "ref_id": "5"
    },
    {
        "id": "35",
        "phase": "3",
        "cycle": "2",
        "start": "361",
        "length": "180",
        "patient_category_id": "3",
        "task_type": "5",
        "task_name": "日常数据",
        "task_title": "监测体温以判断排卵情况（患者自采最低温，临床上已接受使用电子体温计“孕律”）",
        "task_detail": "",
        "ref_id": "5"
    },
    {
        "id": "38",
        "phase": "3",
        "cycle": "2",
        "start": "541",
        "length": "180",
        "patient_category_id": "3",
        "task_type": "5",
        "task_name": "日常数据",
        "task_title": "监测体温以判断排卵情况（患者自采最低温，临床上已接受使用电子体温计“孕律”）",
        "task_detail": "",
        "ref_id": "5"
    },
    {
        "id": "6",
        "phase": "1",
        "cycle": "2",
        "start": "1",
        "length": "1",
        "patient_category_id": "5",
        "task_type": "6",
        "task_name": "膳食营养",
        "task_title": "饮食禁忌",
        "task_detail": "禁食辛辣煎炸之品，以清淡易消化为宜",
        "ref_id": "0"
    },
    {
        "id": "14",
        "phase": "1",
        "cycle": "2",
        "start": "2",
        "length": "3",
        "patient_category_id": "5",
        "task_type": "6",
        "task_name": "膳食营养",
        "task_title": "饮食禁忌",
        "task_detail": "禁食辛辣煎炸之品，以清淡易消化为宜",
        "ref_id": "0"
    },
    {
        "id": "7",
        "phase": "1",
        "cycle": "2",
        "start": "1",
        "length": "1",
        "patient_category_id": "5",
        "task_type": "7",
        "task_name": "运动健身",
        "task_title": "运动健身",
        "task_detail": "全休；禁性生活、盆浴",
        "ref_id": "0"
    },
    {
        "id": "15",
        "phase": "1",
        "cycle": "2",
        "start": "2",
        "length": "3",
        "patient_category_id": "5",
        "task_type": "7",
        "task_name": "运动健身",
        "task_title": "运动健身",
        "task_detail": "全休；禁性生活、盆浴",
        "ref_id": "0"
    },
    {
        "id": "22",
        "phase": "2",
        "cycle": "2",
        "start": "5",
        "length": "13",
        "patient_category_id": "1",
        "task_type": "7",
        "task_name": "运动健身",
        "task_title": "运动健身",
        "task_detail": "全休；禁性生活、盆浴",
        "ref_id": "0"
    },
    {
        "id": "23",
        "phase": "2",
        "cycle": "2",
        "start": "18",
        "length": "13",
        "patient_category_id": "5",
        "task_type": "7",
        "task_name": "运动健身",
        "task_title": "运动健身",
        "task_detail": "禁性生活、盆浴",
        "ref_id": "0"
    },
    {
        "id": "8",
        "phase": "1",
        "cycle": "2",
        "start": "1",
        "length": "1",
        "patient_category_id": "7",
        "task_type": "8",
        "task_name": "健康教育心理支持",
        "task_title": "健康教育、心理支持",
        "task_detail": "http://v.baidu.com/",
        "ref_id": "0"
    },
    {
        "id": "16",
        "phase": "1",
        "cycle": "2",
        "start": "2",
        "length": "3",
        "patient_category_id": "7",
        "task_type": "8",
        "task_name": "健康教育心理支持",
        "task_title": "健康教育、心理支持",
        "task_detail": "http://v.baidu.com/",
        "ref_id": "0"
    },
    {
        "id": "24",
        "phase": "2",
        "cycle": "2",
        "start": "5",
        "length": "26",
        "patient_category_id": "6",
        "task_type": "8",
        "task_name": "健康教育心理支持",
        "task_title": "健康教育、心理支持",
        "task_detail": "http://v.baidu.com/",
        "ref_id": "0"
    },
    {
        "id": "27",
        "phase": "3",
        "cycle": "2",
        "start": "31",
        "length": "60",
        "patient_category_id": "6",
        "task_type": "8",
        "task_name": "健康教育心理支持",
        "task_title": "健康教育、心理支持",
        "task_detail": "http://v.baidu.com/",
        "ref_id": "0"
    },
    {
        "id": "30",
        "phase": "3",
        "cycle": "2",
        "start": "91",
        "length": "90",
        "patient_category_id": "6",
        "task_type": "8",
        "task_name": "健康教育心理支持",
        "task_title": "健康教育、心理支持",
        "task_detail": "http://v.baidu.com/",
        "ref_id": "0"
    },
    {
        "id": "33",
        "phase": "3",
        "cycle": "2",
        "start": "181",
        "length": "180",
        "patient_category_id": "6",
        "task_type": "8",
        "task_name": "健康教育心理支持",
        "task_title": "健康教育、心理支持",
        "task_detail": "http://v.baidu.com/",
        "ref_id": "0"
    },
    {
        "id": "36",
        "phase": "3",
        "cycle": "2",
        "start": "361",
        "length": "180",
        "patient_category_id": "6",
        "task_type": "8",
        "task_name": "健康教育心理支持",
        "task_title": "健康教育、心理支持",
        "task_detail": "http://v.baidu.com/",
        "ref_id": "0"
    },
    {
        "id": "39",
        "phase": "3",
        "cycle": "2",
        "start": "541",
        "length": "180",
        "patient_category_id": "6",
        "task_type": "8",
        "task_name": "健康教育心理支持",
        "task_title": "健康教育、心理支持",
        "task_detail": "http://v.baidu.com/",
        "ref_id": "0"
    }
]

/**
 * 添加数据
 */
exports.add = function () {
    var implListDataCopy = implListData.slice();

    // 在数据头部添加新的数据
    implListDataCopy.unshift({
        "phase": "1",
        "cycle": "2",
        "start": "1",
        "length": "1",
        "patient_category_id": "4",
        "task_type": "1",
        "task_name": "病情评估",
        "task_title": "我加的呀",
        "task_detail": "24小时结束时随访呀呀呀",
        "ref_id": "1"
    });

    var testData = {
        passport_id: '11111',
        rehab_path_tpl_id: '1',
        impl_list: implListDataCopy
    }

    // 发送请求
    j.ajax({
        url: config.API_ENDPOINT + 'modificaterehabpath',
        method: 'POST',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        data: testData
    })
    .done(function(data) {
        callback.call(data, 200, data);
    });
}

/**
 * 修改数据
 */
exports.update = function () {
    var implListDataCopy = implListData.slice();

    // 将头部新添加的新数据修改一下
    implListDataCopy.unshift({
        "id": "1",
        "phase": "1",
        "cycle": "2",
        "start": "1",
        "length": "1",
        "patient_category_id": "4",
        "task_type": "1",
        "task_name": "病情评估",
        "task_title": "我加的呀改的哈",
        "task_detail": "24小时结束时随访呀呀呀改的哈",
        "ref_id": "1"
    });

    var testData = {
        passport_id: '11111',
        rehab_path_tpl_id: '1',
        impl_list: implListDataCopy
    }

    // 发送请求
    j.ajax({
        url: config.API_ENDPOINT + 'modificaterehabpath',
        method: 'POST',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        data: testData
    })
    .done(function(data) {
        console.log(data);
    });
}

/**
 * 删除数据
 */
exports.remove = function () {
    var implListDataCopy = implListData.slice();

    var testData = {
        passport_id: '11111',
        impl_list: implListDataCopy,
        rehab_path_tpl_id: '1',
        // 需要将刚才添加新数据的ID填在这里
        del_id_list: ["1"]
    }

    // 发送请求
    j.ajax({
        url: config.API_ENDPOINT + 'modificaterehabpath',
        method: 'POST',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        data: testData
    })
    .done(function(data) {
        callback.call(data, 200, data);
    });
}

/**
 * 添加、更新、删除数据
 */
exports.all = function () {
    var implListDataCopy = implListData.slice();

    // 在数据头部添加新的数据
    implListDataCopy.unshift({
        "phase": "1",
        "cycle": "2",
        "start": "1",
        "length": "1",
        "patient_category_id": "4",
        "task_type": "1",
        "task_name": "病情评估",
        "task_title": "我加的呀改的哈",
        "task_detail": "24小时结束时随访呀呀呀改的哈",
        "ref_id": "1"
    });

    // 修改第二条数据
    implListDataCopy[1].task_title = '72h结束时系统发起第一阶段评估（发送表一问题4-13）修改哈'
    implListDataCopy[1].task_detail = 'detail修改哈'

    var testData = {
        passport_id: '11111',
        impl_list: implListDataCopy,
        rehab_path_tpl_id: '1',
        // 需要将刚才添加新数据的ID填在这里
        del_id_list: ["1"]
    }

    // 发送请求
    j.ajax({
        url: config.API_ENDPOINT + 'modificaterehabpath',
        method: 'POST',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        data: testData
    })
    .done(function(data) {
        callback.call(data, 200, data);
    });
}
