var superagent = require('superagent');
var config = require('./config');
var j = require('jquery');

/**
 * getpatientrehabpathdetail
 * 获取患者康复路径详情接口
 */
exports.read = function () {
    superagent
        .get(config.API_ENDPOINT + 'getpatientrehabpathdetail')
        .withCredentials()
        .query({
            passport_id: '11111'
        })
        .end(function(err, res){
            console.log(JSON.parse(res.text));
        });
}

/**
 * getpatientrehabpathdetail
 * 获取患者康复路径详情接口
 */
exports.readPretty = function () {
    superagent
        .get(config.API_ENDPOINT + 'getpatientrehabpathdetail')
        .withCredentials()
        .query({
            passport_id: '11111'
        })
        .end(function(err, res){
            var rehabPathDetailData = JSON.parse(res.text).data;
            var impl_list = [];

            for (var name in rehabPathDetailData.rehab_detail_list) {
                for (var order in rehabPathDetailData.rehab_detail_list[name]) {
                    for (var i = 0; i < rehabPathDetailData.rehab_detail_list[name][order].length; i++) {
                        var task = rehabPathDetailData.rehab_detail_list[name][order][i];
                        var taskDataCopy = {};

                        for (var key in task) {
                            taskDataCopy[key] = task[key];
                        }

                        impl_list.push(taskDataCopy);
                    }
                }
            }

            console.log(JSON.stringify(impl_list, null, 4));
        });
}
