var superagent = require('superagent');
var config = require('./config');
var j = require('jquery');


/**
 * 接口类
 * @param {String} name 接口名称
 */
function Api(name) {
    var thisInstance = this;

    thisInstance.name = name;

    thisInstance.post = function (data, callback) {
        // 发送请求
        j.ajax({
            url: config.API_ENDPOINT + thisInstance.name,
            method: 'POST',
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            data: data
        })
        .done(function(data) {
            callback.call(data, 200, data);
        });
    }

    thisInstance.get = function (query, callback) {
        superagent
            .get(config.API_ENDPOINT + thisInstance.name)
            .withCredentials()
            .query(query)
            .end(function(err, res){
                callback.call({}, 200, res.text);
            });
    }
}

/**
 * 导出
 */
module.exports = Api;
